/*
 * local.js (requires jQuery+UI, ace.js, editor.js)
 * @author: cylgalad <cylgalad@gmail.com>
 */

// From http://phpjs.org/functions/htmlspecialchars_decode/
function htmlspecialchars_decode(string, quote_style) {
  //       discuss at: http://phpjs.org/functions/htmlspecialchars_decode/
  //      original by: Mirek Slugen
  //      improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //      bugfixed by: Mateusz "loonquawl" Zalega
  //      bugfixed by: Onno Marsman
  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
  //         input by: ReverseSyntax
  //         input by: Slawomir Kaniecki
  //         input by: Scott Cariss
  //         input by: Francois
  //         input by: Ratheous
  //         input by: Mailfaker (http://www.weedem.fr/)
  //       revised by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // reimplemented by: Brett Zamir (http://brett-zamir.me)
  //        example 1: htmlspecialchars_decode("<p>this -&gt; &quot;</p>", 'ENT_NOQUOTES');
  //        returns 1: '<p>this -> &quot;</p>'
  //        example 2: htmlspecialchars_decode("&amp;quot;");
  //        returns 2: '&quot;'

  var optTemp = 0,
    i = 0,
    noquotes = false;
  if (typeof quote_style === 'undefined') {
    quote_style = 2;
  }
  string = string.toString()
    .replace(/&lt;/g, '<')
    .replace(/&gt;/g, '>');
  var OPTS = {
    'ENT_NOQUOTES': 0,
    'ENT_HTML_QUOTE_SINGLE': 1,
    'ENT_HTML_QUOTE_DOUBLE': 2,
    'ENT_COMPAT': 2,
    'ENT_QUOTES': 3,
    'ENT_IGNORE': 4
  };
  if (quote_style === 0) {
    noquotes = true;
  }
  if (typeof quote_style !== 'number') {
    // Allow for a single string or an array of string flags
    quote_style = [].concat(quote_style);
    for (i = 0; i < quote_style.length; i++) {
      // Resolve string input to bitwise e.g. 'PATHINFO_EXTENSION' becomes 4
      if (OPTS[quote_style[i]] === 0) {
        noquotes = true;
      } else if (OPTS[quote_style[i]]) {
        optTemp = optTemp | OPTS[quote_style[i]];
      }
    }
    quote_style = optTemp;
  }
  if (quote_style & OPTS.ENT_HTML_QUOTE_SINGLE) {
    string = string.replace(/&#0*39;/g, "'"); // PHP doesn't currently escape if more than one 0, but it should
    // string = string.replace(/&apos;|&#x0*27;/g, "'"); // This would also be useful here, but not a part of PHP
  }
  if (!noquotes) {
    string = string.replace(/&quot;/g, '"');
  }
  // Put this in last place to avoid escape being double-decoded
  string = string.replace(/&amp;/g, '&');

  return string;
}

function setTitle(newTitle)
{
    document.title=(newTitle===""?"":newTitle+" - ")+"AceOfSpadeet";
}

function getThemeFromCaption(tl,caption)
{
    var t="";
    for(var thm in tl.themes)
    {
        var cap=tl.themes[thm].caption;
        var isdark=tl.themes[thm].isDark;
        if(caption==cap+(isdark?" (dark)":""))
        {
            t=tl.themes[thm];
            break;
        }
    }
    return t;
}

function getModeFromCaption(ml,caption)
{
    var m="";
    for(var md in ml.modes)
    {
        if(caption==ml.modes[md].caption)
        {
            m=ml.modes[md];
            break;
        }
    }
    return m;
}

function fillThemeMenu(tl,cur)
{
    var i=0;
    for(var thm in tl.themes)
    {
        var th=tl.themes[thm].theme;
        var isdark=tl.themes[thm].isDark;
        var caption=tl.themes[thm].caption+(isdark?" (dark)":"");
        var code="<a href='#' id='theme-"+i+
            "' role='menuitem' tabindex='-1' class='dropdown-item'>";
        if(th==cur)
        {
            code+="<i class='fa fa-check'></i>";
            $("#themeName").text(caption);
        }
        code+=caption+"</a>";
        $("#menuTheme").append(code);
        i++;
    }
}

function fillModeMenu(ml,cur)
{
    var i=0;
    for(var mode in ml.modes)
    {
        var md=ml.modes[mode].mode;
        var caption=ml.modes[mode].caption;
        var code="<a href='#' id='mode-"+i+
            "' role='menuitem' tabindex='-1' class='dropdown-item'>";
        if(md==cur)
        {
            code+="<i class='fa fa-check'></i>";
            $("#modeName").text(caption);
        }
        code+=caption+"</a>";
        $("#menuMode").append(code);
        i++;
    }
}

$(document).ready(function()
{
    var aceThemeName="ace/theme/tomorrow_night";
    var aceModeName="ace/mode/html";

    ace.require("ace/ext/language_tools");
    var editor=ace.edit("editor");
    var StatusBar=ace.require("ace/ext/statusbar").StatusBar;
    var statusBar=new StatusBar(editor,document.getElementById("statusBar"));
    var modeList=ace.require("ace/ext/modelist");
    var themeList=ace.require("ace/ext/themelist");

    $("#editor").css("font-size","22px");
    editor.setAutoScrollEditorIntoView(true);
    editor.setOptions(
    {
        enableBasicAutocompletion: true,
        enableSnippets: true,
        maxLines: 24,
        minLines: 24
    });
    editor.setShowPrintMargin(true);
    editor.setHighlightActiveLine(true);
    editor.setShowInvisibles(true);

    editor.setTheme(aceThemeName);
    editor.getSession().setMode(aceModeName);

    editor.commands.addCommand(
    {
        name: "New",
        bindKey: {win: "Ctrl-N",  mac: "Command-N"},
        exec: function(editor)
        {
            $("#btn_new").click();
        },
        readOnly: true
    });
    editor.commands.addCommand(
    {
        name: "Load",
        bindKey: {win: "Ctrl-L",  mac: "Command-L"},
        exec: function(editor)
        {
            $("#btn_load").click();
        },
        readOnly: true
    });
    editor.commands.addCommand(
    {
        name: "Save",
        bindKey: {win: "Ctrl-S",  mac: "Command-S"},
        exec: function(editor)
        {
            $("#btn_save").click();
        },
        readOnly: true
    });
    editor.commands.addCommand(
    {
        name: "Search",
        bindKey: {win: "F3",  mac: "F3"},
        exec: function(editor)
        {
            $("#btn_search").click();
        },
        readOnly: true
    });

    fillThemeMenu(themeList,aceThemeName);
    fillModeMenu(modeList,aceModeName);

    $("#btn_new").click(function()
    {
        editor.setValue("");
        $("#fileName").empty();
        setTitle("");
        $("#btn_load").val("");
        $("#result").empty();
    });

    $("#btn_save").click(function()
    {
        var txt=editor.getValue();
        var filename=$("#btn_load").val().split('\\').pop();
        $.post("toolbar/save.php",{"text": txt,"filename": filename})
            .done(function(data)
            {
                var dlif=$("<iframe/>",{"src":data}).hide();
                $("body").append(dlif);
            });

        $("#fileName").text(filename);
        setTitle(filename);
        $("#btn_load").val(filename);
    });

    $("#btn_show").click(function()
    {
        var txt=editor.getValue();
        $("#result").empty().append(htmlspecialchars_decode(txt));
    });

    $("#btn_unshow").click(function()
    {
        $("#result").empty();
    });

    // Thanks to http://stackoverflow.com/a/26298948
    function readSingleFile(e)
    {
        var file=e.target.files[0];
        if(!file)
        {
            return;
        }
        var reader=new FileReader();
        reader.onload=function(e)
        {
            var contents=e.target.result;
            editor.setValue(contents);
            editor.clearSelection();
        };
        reader.readAsText(file);

        $("#fileName").text(file.name);
        setTitle(file.name);
        $("#result").empty();

        var mode=modeList.getModeForPath(file.name);
        editor.session.setMode(mode.mode);
        var oldmd=$("#modeName").text();
        $("a[id^='mode']:has(i)").html(oldmd);
        $("a[id^='mode']").filter(function()
            {
                return $(this).text()===mode.caption;
            }).html("<i class='fa fa-check'></i>"+mode.caption);
        $("#modeName").text(mode.caption);

        editor.focus();
    }

    $("#btn_load").change(readSingleFile);

    $("a[id^='theme']").click(function()
    {
        var th=$(this).text();
        var oldth=$("#themeName").text();
        var thm=getThemeFromCaption(themeList,th);
        editor.setTheme(thm.theme);
        $("a[id^='theme']:has(i)").html(oldth);
        $(this).html("<i class='fa fa-check'></i>"+th);
        $("#themeName").text(th);
    });

    $("a[id^='mode']").click(function()
    {
        var md=$(this).text();
        var oldmd=$("#modeName").text();
        var mod=getModeFromCaption(modeList,md);
        editor.getSession().setMode(mod.mode);
        $("a[id^='mode']:has(i)").html(oldmd);
        $(this).html("<i class='fa fa-check'></i>"+md);
        $("#modeName").text(md);
    });

    function setEditorFontSize()
    {
        var fs=parseInt($("#editor").css("font-size"),10);
        $("#fontSize").text(fs);
    }

    $("#btn_fontsmaller").click(function()
    {
        $("#editor").css(
        {
            "font-size": function(index,value)
            {
                return parseFloat(value)-1.0;
            }
        });
        setEditorFontSize();
    });

    $("#btn_fontbigger").click(function()
    {
        $("#editor").css(
        {
            "font-size": function(index,value)
            {
                return parseFloat(value)+1.0;
            }
        });
        setEditorFontSize();
    });

    $("#btn_search").click(function()
    {
        editor.execCommand("find");
    });

    $("#btn_searchN").click(function()
    {
        editor.findNext();
    });

    $("#btn_searchP").click(function()
    {
        editor.findPrevious();
    });
});
