<?php
// download thru ajax
// Thanks to http://stackoverflow.com/a/11452917

$fn=$_GET["file"];
if($fn)
{
    $fnr="../tmp/".htmlspecialchars($fn);
    if(file_exists($fnr))
    {
        header("Content-Description: File Transfer");
        header("Content-Type: application/force-download");
        header("Content-Disposition: attachment; filename=".basename($fnr));
        header("Content-Transfer-Encoding: binary");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Pragma: public");
        header("Content-Length: ".filesize($fnr));
        ob_clean();
        flush();
        readfile($fnr);
        @unlink($fnr);
    }
}
?>