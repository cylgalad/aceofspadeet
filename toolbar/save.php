<?php
session_start();

if(array_key_exists("text",$_POST))
{
    $txt=htmlspecialchars_decode($_POST["text"]);
    if(array_key_exists("filename",$_POST))
    {
        $filename=$_POST["filename"];
    }
    else
    {
        $filename="new_document.html";
    }
    if($filename=="")
    {
        $filename="document.html";
    }

    file_put_contents("../tmp/$filename",$txt);
    echo "rqst/dlfile.php?file=$filename";
}
?>