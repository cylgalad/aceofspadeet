# Ace Of Spade Edit (aceofspadeet) #

An [ACE-based](http://ace.c9.io/#nav=about) editor, using [Bootstrap](http://getbootstrap.com/), [Font-Awesome](https://fontawesome.com/), [jQuery](https://jquery.com/) and of course [PHP](https://php.net/).
